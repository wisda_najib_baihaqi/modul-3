import React from "react";
import { Button, Text, View } from "react-native";

export const LoginScreen = ({ navigation }) => {
  return (
    <View>
      <Text>Login Screen</Text>
      <Button
        title="Go to Drawer"
        onPress={() => {
          navigation.push("Drawer");
        }}
      />
    </View>
  );
};
